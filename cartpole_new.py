"""
Classic cart-pole system implemented by Rich Sutton et al.
Copied from http://incompleteideas.net/sutton/book/code/pole.c
permalink: https://perma.cc/C9ZM-652R
"""

import math
import gym
from gym import spaces, logger
from gym.utils import seeding
import numpy as np
from copy import deepcopy
from gym.envs.classic_control import rendering


class CartPoleNewEnv(gym.Env):
    metadata = {
        'render.modes': ['human', 'rgb_array'],
        'video.frames_per_second': 50
    }

    def __init__(self):
        self.gravity = 9.8
        self.masscart = 1.0
        self.masspole = 0.1
        self.total_mass = (self.masspole + self.masscart)
        self.length = 0.5  # actually half the pole's length
        self.polemass_length = (self.masspole * self.length)
        self.force_mag = 10.0
        self.tau = 0.02  # seconds between state updates #TODO change this here to 0.1 for ex.
        # TODO write method that goes 5 steps instead of 1 (so that we get 0.02*5=0.1)

        # Angle at which to fail the episode
        self.theta_threshold_radians = 12 * 2 * math.pi / 360
        self.x_threshold = 2.4

        # Angle limit set to 2 * theta_threshold_radians so failing observation is still within bounds
        high = np.array([
            self.x_threshold * 2,
            np.finfo(np.float32).max,
            self.theta_threshold_radians * 2,
            np.finfo(np.float32).max])

        self.action_space = spaces.Discrete(2)
        self.observation_space = spaces.Box(-high, high)

        self.seed()
        self.viewer = None
        self.state = None
        self.action=None #TODO added action here so that we can get it below for rendering
        self.leftright=None #TODO added for rendering of direction
        self.state_discretized = None
        self.state_discretized_index = None
        self.steps_beyond_done = None
        self.lower_bounds = (self.observation_space.low[0], -0.5, self.observation_space.low[2], -np.math.radians(50))
        self.upper_bounds = (self.observation_space.high[0], 0.5, self.observation_space.high[2], np.math.radians(50))
        self.num_buckets = 30  # TODO: change this here to more buckets to improve

        # TODO done: here added variables from the render class
        self.screen_width = 600
        self.screen_height = 400
        self.world_width = self.x_threshold * 2
        self.scale = self.screen_width / self.world_width
        self.carty = 100  # TOP OF CART
        # added and this is updated below
        self.cartx = 300
        self.polewidth = 10.0
        self.polelen = self.scale * 1.0
        self.cartwidth = 50.0
        self.cartheight = 30.0

        # add transobjects
        self.carttrans = None
        self.buckettrans = None
        self.anglebuckettrans = None
        self.tenanglebuckettrans = None
        self.poletrans = None

    def seed(self, seed=None):
        self.np_random, seed = seeding.np_random(seed)
        return [seed]

        # TODO discretize method but leave the xpos continuous

    def discretize(self, state):
        '''

        :param state:state (4vector) we want to discretize
        :return:discretized state and bucketindex of the discretized state
        '''
        stepsize = [(self.upper_bounds[i] - self.lower_bounds[i]) / self.num_buckets for i in range(len(state))]
        self.state_discretized_index = [(np.floor((state[i] - self.lower_bounds[i]) / stepsize[i])) for i in
                                        range(1, len(state))]
        self.state_discretized_index = [max(self.state_discretized_index[i], 0) for i in range(len(state) - 1)]
        self.state_discretized_index = [min(self.state_discretized_index[i], self.num_buckets - 1) for i in
                                        range(len(state) - 1)]

        self.state_discretized = [min(self.upper_bounds[i + 1],
                                      ((self.state_discretized_index[i] + 0.5) * stepsize[i + 1]) + self.lower_bounds[
                                          i + 1]) for i in range(len(state) - 1)]
        self.state_discretized = [max(self.state_discretized[i], self.lower_bounds[i + 1]) for i in
                                  range(len(state) - 1)]

        statearray = np.zeros(len(state))
        statearray[0] = state[0]
        for i in range(1, len(state)):
            statearray[i] = self.state_discretized[i - 1]
        self.state_discretized = statearray
        return self.state_discretized, self.state_discretized_index

    def discretize_back(self, state):
        '''
        this method gives us a real state out of a tuple state (buckets)
        :param state:state as a tuple which we want as a real state in here state[i] denotes the bucketnumber
        :return:state in tupleform but with coordinates
        '''

        lower_bounds = [-0.5, -0.418, -np.math.radians(50)]
        upper_bounds = [0.5, 0.418, np.math.radians(50)]
        stepsize = [(upper_bounds[i] - lower_bounds[i]) / self.num_buckets for i in range(len(state))]
        real_state_temp = [((state[i] + 0.5) * stepsize[i]) for i in range(len(state))]
        real_state = [real_state_temp[i] + lower_bounds[i] for i in range(len(state))]
        # TODO we need additionally an xpos of the cart

        real_statearray = np.zeros(len(self.state))
        real_statearray[0] = self.state[0]
        for i in range(1, len(self.state)):
            real_statearray[i] = real_state[i - 1]
        return real_statearray

    def multiplesteps_in_actiondir(self, action, num_actionsteps=3):
        '''
        this should perform 5 times the action that was choosen
        :param action:action that we want to perform multiple times
        :param num_actionsteps: number of steps with this action we take
        :return:
        '''
        for i in range(num_actionsteps):
            self.state, reward, done, info = self.env.step(action)
        return self.state, reward, done, info

    def step(self, action):
        assert self.action_space.contains(action), "%r (%s) invalid" % (action, type(action))
        state = self.state
        self.action=action #TODO added so that we can use this below
        x, x_dot, theta, theta_dot = state
        force = self.force_mag if self.action == 1 else -self.force_mag #TODO changed action to self.action
        costheta = math.cos(theta)
        sintheta = math.sin(theta)
        temp = (force + self.polemass_length * theta_dot * theta_dot * sintheta) / self.total_mass
        thetaacc = (self.gravity * sintheta - costheta * temp) / (
                    self.length * (4.0 / 3.0 - self.masspole * costheta * costheta / self.total_mass))
        xacc = temp - self.polemass_length * thetaacc * costheta / self.total_mass
        x = x + self.tau * x_dot
        x_dot = x_dot + self.tau * xacc
        theta = theta + self.tau * theta_dot
        theta_dot = theta_dot + self.tau * thetaacc
        self.state = (x, x_dot, theta, theta_dot)
        # TODO change this here so that done only take into account the angle and not the xposition
        done = theta < -self.theta_threshold_radians \
               or theta > self.theta_threshold_radians
        '''done =  x < -self.x_threshold \
                or x > self.x_threshold \
                or theta < -self.theta_threshold_radians \
                or theta > self.theta_threshold_radians
                '''
        done = bool(done)

        reward = 1.0 #TODO

        if not theta <-math.radians(3) and not theta > math.radians(3):
            reward= 1.0
        elif not done:
            reward=0.0

        elif self.steps_beyond_done is None:
            self.steps_beyond_done= 0
            reward = -1.0
        else:
            if self.steps_beyond_done==0:
                logger.warn("you are callling step even though this environment has already returned done=True. You should always call reeset() once you receive done=true ")
                self.steps_beyond_done +=1
                reward =-1.0


        #if done:
         #   reward = 0.0

        # TODO added discretized state saved
        discr_state, discrindex = self.discretize(self.state)
        self.state_discretized = discr_state
        self.state_discretized_index = discrindex

        # TODO ended discretized state added

        return np.array(self.state), reward, done, {}

    def is_done(self):
        # Function that returns true if done= True
        x, x_dot, theta, theta_dot = self.state
        done = theta < -self.theta_threshold_radians \
               or theta > self.theta_threshold_radians
        done = bool(done)
        return done

    def tuple_to_scalar(self):  # TODO change num_buckets here or pass another value to the tupletoscalarmethod
        '''
        this tuple_to_scalar method works for all bucketsizes
        method that transforms a 4tuple input into a scalar
        :param state: 4tuple state as input, gets transformed to a scalar
        :return: scalar state
        '''
        # TODO changed here self.state_discretized_index instead of state
        potence = np.math.log(self.num_buckets, 10)  # log with basis 10
        scalar_state = (100 ** potence) * self.state_discretized_index[0] + (10 ** potence) * \
                       self.state_discretized_index[1] + (
                               1 ** potence) * self.state_discretized_index[2]
        # scalar_state = (1000 ** potence) * self.state[0] + (100 ** potence) * self.state[1] + (10 ** potence) * self.state[2] + (
        #           1 ** potence) * self.state[3]
        # scalar_state= 1000*state[0]+100*state[1]+10*state[2] +1*state[3]
        # tuple= [scalar_state/1000, scalar_state%1000, scalar_state%100, scalar_state%10] # this maps back scalars to tuplestate
        return scalar_state

    def scalar_to_tuple(self, scalar):
        '''
        takes a scalar state and maps it to a 4tuplestate
        :param scalar: input we want to have in a 4tuple
        :return: 4vector of the input
        '''
        state = [((scalar - (scalar % self.num_buckets ** 2)) / self.num_buckets ** 2),
                 (((scalar % self.num_buckets ** 2) - (scalar % self.num_buckets)) / self.num_buckets),
                 (scalar % self.num_buckets)]
        return state

    def set_state(self, old_state):
        '''
        this method should from single index state get the tuple
        and from the tuple get the middle of the buckets
        :param old_state:
        :return:
        '''
        new_state = old_state
        tuple_state = self.scalar_to_tuple(new_state)  # makes a tuple out of the inputscalar
        real_state = self.discretize_back(tuple_state)  # this makes a state out of the tuple using discretize_back
        self.state = real_state  # TODO

    def reset(self):
        self.state= np.random.uniform(low= 0, high= 0.2, size=(4,))
        #self.state = self.np_random.uniform(low=-0.05, high=0.05, size=(4,))
        self.steps_beyond_done = None
        return np.array(self.state)

    def world_to_screen_coord(self, world_x, x_range=9.6):
        '''
        transform world coordinates to screen coordinates
        :param world_x: x pos in world coordinates
        :param screen_width:
        :param screen_height:
        :param x_range: range of x pos from -4.8 to 4.8 (thus range is 9.6)
        :return: x pos in screencoordinates (x,y) --> (x',y')= (x+300, y + 200)
        (screen_width/2)/x_range/2 * x'
        '''
        temp = (self.screen_width / 2)
        screen_x = (((self.screen_width / 2) / (x_range / 2)) * world_x) + temp
        return screen_x

    def get_angle_pos(self, current_angle, movingscalex=300, movingscaley=100, poleheight=125):
        '''
        this function should get of position in screencoord when we have an angle
        :param angle: angle we want to have in screencoord
        :param movingscalex: normally 300 (x pos of cart)
        :param movingscaley: initially 100 (y pos of cart)
        :param poleheight: 125 length of pole, used to compute the screenpos of the angle
        :return:angle_pos meaning the screenposition of the cart at the current angle
        '''
        vector = [0, poleheight]
        Rotationmatrix = [[np.cos(current_angle), -np.sin(current_angle)],
                          [np.sin(current_angle), np.cos(current_angle)]]
        angle_pos = np.dot(vector, Rotationmatrix)
        angle_pos[0] = angle_pos[0] + movingscalex
        angle_pos[1] = angle_pos[1] + movingscaley
        return angle_pos

    def corner_init(self):
        lowerleftcorner = (0, 0)
        upperleftcorner = (0, 5)
        lowerrightcorner = (5, 0)
        upperrightcorner = (5, 5)
        polygon_corners = [lowerleftcorner, upperleftcorner, upperrightcorner, lowerrightcorner]
        return polygon_corners

    def get_screen_bucket(self):
        screen_bucket = ((self.screen_width / self.num_buckets) * (self.state_discretized_index[0] + 0.5))
        return screen_bucket

    def render_cartxpos(self):
        cartxbucket = self.get_screen_bucket()
        self.buckettrans.set_translation(cartxbucket, self.carty - 50)

    def render_tenanglebuckets(self):
        for i in range(self.num_buckets + 1):
            delta_alpha = (self.upper_bounds[2] - self.lower_bounds[2]) / self.num_buckets
            angle_i = self.lower_bounds[2] + i * delta_alpha
            tip_point = self.get_angle_pos(angle_i, 300)
            delta_x = self.cartx - 300
            self.tenanglebuckettrans[i].set_translation(tip_point[0] + delta_x, tip_point[1])

    def render_currentanglebucket(self, cartx):
        delta_alpha = (self.upper_bounds[2] - self.lower_bounds[2]) / self.num_buckets
        angle_current = self.lower_bounds[2] + (self.state_discretized_index[2] + 0.5) * delta_alpha
        # 0 angle straight up: we need to add PI/2 to angle for correct rendering
        angleposcurrent = self.get_angle_pos(current_angle=angle_current, movingscalex=cartx)
        self.anglebuckettrans.set_translation(angleposcurrent[0], angleposcurrent[1])

    def update_continuous(self):
        if self.state is None: return None

        x = self.state
        self.cartx = x[0] * self.scale + self.screen_width / 2.0  # MIDDLE OF CART
        self.carttrans.set_translation(self.cartx, self.carty)
        self.poletrans.set_rotation(-x[2])

        # This three methods added by me
        self.render_cartxpos()
        self.render_currentanglebucket(self.cartx)
        self.render_tenanglebuckets()

        # TODO: add rendering for direction the cart takes
        # TODO render line red when action 0, line green when action 1
        #'''
        from gym.envs.classic_control import rendering
        #self.actionlinered = rendering.Line((300, 200), (400, 200))
        #self.actionlinered.set_color(100, 0, 0)
        l, r, t, b = -self.cartwidth / 2+ 200, self.cartwidth / 2 + 200, self.cartheight / 2 + 200, -self.cartheight / 2 + 200
        self.actionpolygonred=rendering.FilledPolygon(
            [((l / 4), (b / 4)), ((l / 4), (t / 4)), ((r / 4), (t / 4)), ((r / 4), (b / 4))])
        self.actionpolygonred.set_color(100,0,0)
        self.actionpolygongreen = rendering.FilledPolygon(
            [((l / 4), (b / 4)), ((l / 4), (t / 4)), ((r / 4), (t / 4)), ((r / 4), (b / 4))])
        self.actionpolygongreen.set_color(0,100,0)
        #self.actionlinegreen = rendering.Line((300, 250), (400, 250))
        #self.actionlinegreen.set_color(0, 100, 0)  # green line
        if self.leftright == 0:
            #self.viewer.add_geom(self.actionlinered)
            self.viewer.add_geom(self.actionpolygonred)
        elif self.leftright == 1:
            self.viewer.add_geom(self.actionpolygongreen)
            #self.viewer.add_geom(self.actionlinegreen)

        # TODO actionline end
       # '''

        # TODO here set continuousstate to discretestate
        self.state_discretized_copy = deepcopy(self.state_discretized)
        self.state = self.state_discretized_copy
        # TODO set continuousstate to discretestate end

    def update_discrete(self):
        if self.state is None: return None

        x = self.state
        self.cartx = x[0] * self.scale + self.screen_width / 2.0  # MIDDLE OF CART
        self.carttrans.set_translation(self.cartx, self.carty)
        self.poletrans.set_rotation(-x[2])

    def render(self, mode='human'):
        def init_cont__(self):

            if self.viewer is None:
                from gym.envs.classic_control import rendering
                self.viewer = rendering.Viewer(self.screen_width, self.screen_height)
                l, r, t, b = -self.cartwidth / 2, self.cartwidth / 2, self.cartheight / 2, -self.cartheight / 2
                axleoffset = self.cartheight / 4.0
                cart = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
                self.carttrans = rendering.Transform()
                cart.add_attr(self.carttrans)
                self.viewer.add_geom(cart)
                current_bucket_polygon = rendering.FilledPolygon(
                    [((l / 4), (b / 4)), ((l / 4), (t / 4)), ((r / 4), (t / 4)), ((r / 4), (b / 4))])
                self.buckettrans = rendering.Transform()
                current_bucket_polygon.add_attr(self.buckettrans)
                self.viewer.add_geom(current_bucket_polygon)

                # initialization  for currenanglebucket (blue polygon)
                polygon_corners = self.corner_init()
                current_angle_bucket_polygon = rendering.FilledPolygon(polygon_corners)
                current_angle_bucket_polygon.set_color(0, 100, 0)
                self.anglebuckettrans = rendering.Transform()
                current_angle_bucket_polygon.add_attr(self.anglebuckettrans)
                self.viewer.add_geom(current_angle_bucket_polygon)

                




                # initialization for the rendering of tenanglebuckets
                self.tenanglebuckettrans = []
                for i in range(self.num_buckets + 1):
                    polygon_corners = self.corner_init()
                    ten_angle_bucket_polygon = rendering.FilledPolygon(polygon_corners)
                    self.tenanglebuckettrans.append(rendering.Transform())
                    ten_angle_bucket_polygon.add_attr(self.tenanglebuckettrans[-1])
                    self.viewer.add_geom(ten_angle_bucket_polygon)

                # Rendering of ten red buckets for xposition of cart
                for i in range(self.num_buckets + 1):
                    lower_bound_buckets = self.world_to_screen_coord(-4.8)
                    upper_bound_buckets = self.world_to_screen_coord(4.8)
                    screen_bucketsize = (upper_bound_buckets - lower_bound_buckets) / self.num_buckets
                    self.bucketline = rendering.Line(((upper_bound_buckets - i * (screen_bucketsize)), 50),
                                                     ((upper_bound_buckets - i * (screen_bucketsize)), 100))
                    self.bucketline.set_color(100, 0, 0)
                    self.viewer.add_geom(self.bucketline)

                # TODO ended
                l, r, t, b = -self.polewidth / 2, self.polewidth / 2, self.polelen - self.polewidth / 2, -self.polewidth / 2
                pole = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
                pole.set_color(.8, .6, .4)
                self.poletrans = rendering.Transform(translation=(0, axleoffset))
                pole.add_attr(self.poletrans)
                pole.add_attr(self.carttrans)
                self.viewer.add_geom(pole)
                self.axle = rendering.make_circle(self.polewidth / 2)
                self.axle.add_attr(self.poletrans)
                self.axle.add_attr(self.carttrans)
                self.axle.set_color(.5, .5, .8)
                self.viewer.add_geom(self.axle)

                self.track = rendering.Line((0, self.carty), (self.screen_width, self.carty))
                self.track.set_color(0, 0, 0)  # horizontal line that goes through the middle of the cart
                self.viewer.add_geom(self.track)



        def init_discrete(self):
            if self.viewer is None:
                from gym.envs.classic_control import rendering
                self.viewer = rendering.Viewer(self.screen_width, self.screen_height)
                l, r, t, b = -self.cartwidth / 2, self.cartwidth / 2, self.cartheight / 2, -self.cartheight / 2
                axleoffset = self.cartheight / 4.0
                cart = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
                self.carttrans = rendering.Transform()
                cart.add_attr(self.carttrans)
                self.viewer.add_geom(cart)
                l, r, t, b = -self.polewidth / 2, self.polewidth / 2, self.polelen - self.polewidth / 2, -self.polewidth / 2
                pole = rendering.FilledPolygon([(l, b), (l, t), (r, t), (r, b)])
                pole.set_color(.8, .6, .4)
                self.poletrans = rendering.Transform(translation=(0, axleoffset))
                pole.add_attr(self.poletrans)
                pole.add_attr(self.carttrans)
                self.viewer.add_geom(pole)
                self.axle = rendering.make_circle(self.polewidth / 2)
                self.axle.add_attr(self.poletrans)
                self.axle.add_attr(self.carttrans)
                self.axle.set_color(.5, .5, .8)
                self.viewer.add_geom(self.axle)
                self.track = rendering.Line((0, self.carty), (self.screen_width, self.carty))
                self.track.set_color(0, 0, 0)
                self.viewer.add_geom(self.track)



        discr_state, discrindex = self.discretize(self.state)
        self.state_discretized = discr_state
        self.state_discretized_index = discrindex

        init_cont__(self)
        self.update_continuous()

        # init_discrete(self)
        # self.update_discrete()

        return self.viewer.render(return_rgb_array=mode == 'rgb_array')

    def close(self):
        if self.viewer: self.viewer.close()