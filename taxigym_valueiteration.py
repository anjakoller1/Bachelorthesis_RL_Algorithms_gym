import gym
import numpy as np
import time
import random
from IPython.display import clear_output
# observation_space= discrete(500), action_space: discrete(6) --> left, right, up, down, pickup, dropoff
#  observation_space are all the possible positions of the taxi and the human on the board (5x5x5x4)

#Alg sutton page 67 value iteration

# parameters
gamma = 0.6 # discountfactor from the algorithm
convergence_parameter = 0.000001  #  boundary at which we stop (convergence enough)
# implement a function that gives us best reward/time_steps

# for plotting metrics
all_epochs = []
all_penalties = []


#env = gym.make('FrozenLake8x8-v0').env
env = gym.make('Taxi-v2').env # test with different environments
env.reset()
env.render()
print('number of states in environment is {}'.format(env.nS))
print('number of actions in the environment is {}'.format(env.nA))
# print('transition probabilities of the environment'.format(env.P))
# env.P[s][a] is a list of transition tuples (probability, next_state, reward, done)


V = np.zeros(env.nS) # zero initialization of V
#print(V) # zero vector of states
policy = np.zeros([env.nS, env.nA]) # initialization of policy
penalties, reward = 0,0
total_penalties = 0
round = 0

#  algorithm converges no matter what the initial value for V is
#  algorithm input: s, a, p , r, convergence_parameter
#  meaning set of states, set of actions, p state-transition function P(s'|s,a), reward, and a threshold at which we stop iteration
# output: approximately optimal policy p(S), value function V(S)
# V0(S) assigned arbitrarily, k=0, we iterate over kuntil V(K)-V(k-1)< convergence_parameter
#  we do compute Vk(S) as max over the actions of the sum over states of (P(s'|s,a)*(R(s,a,s')+ gamma*Vk-1(s'))
#  for each state the policy pol(s)  which is the maximum over teh V's and
#  we return the approximately optimal policy pol and the value fucntion Vk

def helperfunction(state,V):
    #  this helper function calculates the value for all actions in a given state
    A = np.zeros(env.nA) #  A is a vector of length env.nA which contains the expected
    #  value of each action
    # env.P are the transition probabilities of the environment
    total__reward=0
    for a in range(env.nA):
        for prob, next_state, reward, done in env.P[state][a]:
            A[a] += prob *(reward + gamma*V[next_state])

    return A

while True:
    delta= 0

    for s in range(env.nS):
        start_time= time.time()     #  measure runtime of one round
        V0= V[s].copy()
        V[s]= max(helperfunction(s,V))
        #delta = max(delta, abs(V0-V[s]))
        delta = np.sum(np.abs(V0-V[s]))
       # policy [s, V[s]]=1 # always take the best action
        action_to_take = np.argmax(helperfunction(s,V))
        env.step(action_to_take)
        env.render() # just to see if it performs the right steps
        round += 1



    #end_time= time.time() # measure runtime of one round
    #time_passed= end_time-start_time
   # print('Time per round:{}'.format(time_passed))
    print('Round number {}'.format(round))

    if delta < convergence_parameter:
        print('valueiteration converged at iteration {}'.format(round))
        break

    #print('the optimal value function is {}'.format(V))


for s in range(env.nS):
    #in value iteration another round is needed to get the policy using the optimal value function
    bestaction = np.argmax((helperfunction(s,V)))
    policy[s, bestaction] = 1.0
   # print('the opimal policy is {}'.format(policy)) # the row we see with the 1s inside is the action we have to take

    # the reshaped grid policy (0= south, 1= nord, 2=east, 3= west, 4= pickup, 5= dropoff)
   # print('The reshaped grid policy is'.format(np.reshape(policy, axis=1), env.shape))

#env.step(bestaction)


total_time_end= time.time()
total_time= total_time_end-start_time

env.render() # render environment to see if the solution really worked


#print('Average penalties per round {}'.format(total_penalties/round))
print('average time per round {}'.format(total_time/round))
print('Total time {}'.format(total_time))

#  create policy and return policy and value function V

print("Training finished.\n")
