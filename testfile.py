import numpy as np


def discretize(state, num_buckets):
    '''

    :param state:state (4vector) we want to discretize
    :return:discretized state and bucketindex of the discretized state
    '''
    lower_bounds= [-4.8, -0.5, -0.418 , -np.math.radians(50)]
    upper_bounds= [4.8, 0.5, 0.418 , np.math.radians(50)]
    stepsize = [(upper_bounds[i] - lower_bounds[i]) / num_buckets for i in range(len(state))]
    state_discretized_index = [(np.floor((state[i] - lower_bounds[i]) / stepsize[i])) for i in
                                    range(len(state))]
    state_discretized_index = [max(state_discretized_index[i], 0) for i in range(len(state))]
    sstate_discretized_index = [min(state_discretized_index[i], num_buckets - 1) for i in
                                    range(len(state))]
    state_discretized = [
        min(upper_bounds[i], ((state_discretized_index[i] + 0.5) * stepsize[i]) + lower_bounds[i]) for i
        in range(len(state))]
    state_discretized = [max(state_discretized[i], lower_bounds[i]) for i in range(len(state))]
    return state_discretized, state_discretized_index




def tuple_to_scalar_3tuple(state, num_buckets):
    '''
    this tuple_to_scalar method works for all bucketsizes
    method that transforms a 4tuple input into a scalar
    :param state: 4tuple state as input, gets transformed to a scalar
    :return: scalar state
    '''
    potence= np.math.log(num_buckets, 10) #log with basis 10
    scalar_state = (100 ** potence) * state[0] + (10 ** potence) * state[1] + (
                1 ** potence) * state[2]
    #TODO: this removes the first state (cartxpos) as this is not so important
    #scalar_state= (1000**potence) *state[0]+(100**potence)*state[1]+(10**potence)*state[2] +(1**potence)*state[3]
    #scalar_state= 1000*state[0]+100*state[1]+10*state[2] +1*state[3]
    #tuple= [scalar_state/1000, scalar_state%1000, scalar_state%100, scalar_state%10] # this maps back scalars to tuplestate
    return scalar_state




def discretize3state(state, num_buckets):
    '''

    :param state:state (4vector) we want to discretize
    :return:discretized state and bucketindex of the discretized state
    '''
    lower_bounds = [-0.5, -0.418, -np.math.radians(50)]
    upper_bounds = [0.5, 0.418, np.math.radians(50)]
    stepsize = [(upper_bounds[i] - lower_bounds[i]) / num_buckets for i in range(len(state))]
    state_discretized_index = [(np.floor((state[i] - lower_bounds[i]) / stepsize[i])) for i in
                                    range(len(state))]
    state_discretized_index = [max(state_discretized_index[i], 0) for i in range(len(state))]
    state_discretized_index = [min(state_discretized_index[i], num_buckets - 1) for i in
                                    range(len(state))]

    state_discretized = [min(upper_bounds[i],
                                  ((state_discretized_index[i] + 0.5) * stepsize[i]) + lower_bounds[
                                      i]) for i in range(len(state))]
    state_discretized = [max(state_discretized[i], lower_bounds[i]) for i in range(len(state))]

    return state_discretized, state_discretized_index


def discretize_back(state, num_buckets):
    '''
    this method gives us a real state out of a tuple state (buckets)
    :param state:state as a tuple which we want as a real state in here state[i] denotes the bucketnumber
    :return:state in tupleform but with coordinates
    '''
    lower_bounds = [-0.5, -0.418, -np.math.radians(50)]
    upper_bounds = [0.5, 0.418, np.math.radians(50)]
    stepsize = [(upper_bounds[i] - lower_bounds[i]) / num_buckets for i in range(len(state))]
    print('stepsize is {}'.format(stepsize))
    #temp= [((state[i]+ 0.5))*stepsize[i]) for i in range(len(state))]
    real_state_temp= [((state[i] + 0.5) * stepsize[i]) for i in range(len(state))]
    real_state= [real_state_temp[i] + lower_bounds[i] for i in range(len(state))]
    #real_state = [(((state[i] + 0.5) * stepsize[i])+lower_bounds[i]) for i in range(len(state))]
    return real_state

def scalar_to_tuple(scalar, num_buckets):
    '''
    takes a scalar state and maps it to a 3tuplestate
    :param scalar: input we want to have in a 3tuple
    :return: 4vector of the input
    '''
    state = [((scalar - (scalar % num_buckets**2))/num_buckets**2), (((scalar %num_buckets**2)-(scalar % num_buckets))/num_buckets), (scalar % num_buckets)]
    return state

scalarstate= 436 #input scalar state
NUM_BUCKETS= 10
print('scalarstate {}'.format(scalarstate))
tuplestate= scalar_to_tuple(scalarstate, num_buckets=NUM_BUCKETS) #tuple this should result in [4,3,6]
print('tuplestate {}'.format(tuplestate))
temp= discretize_back(tuplestate, num_buckets=NUM_BUCKETS) #this gives us the realstate value [0.14,0,0.74]
print('discretized back tuplestate {}'.format(temp))
scalarnew_discr, scalarnew_discrindex= discretize3state(temp, num_buckets=NUM_BUCKETS) #this gives us the discretized real state and its buckets --> [4,3,6]
print('scalarnew_discr {} and scalarnew_discrindex {}'.format(scalarnew_discr,scalarnew_discrindex))
scalarnew= tuple_to_scalar_3tuple(scalarnew_discrindex, num_buckets=NUM_BUCKETS) #this transforms our tuple back to a scalar --> 436
print('scalarnew {}'.format(scalarnew))
#print('tupletoscalar of scalarstate {}'.format(tuple_to_scalar_3tuple(tuplestate, num_buckets=10)))