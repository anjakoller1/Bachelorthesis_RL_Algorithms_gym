import numpy as np

import gym
env = gym.make('CartPole-v0').env
env.reset()
print('observationspace low {}'.format(env.observation_space.low))
print('observationspace high {}'.format(env.observation_space.high))
print(np.math.radians(50))#0.87

LOWER_BOUNDS_SIMPLE = float(-3)
UPPER_BOUNDS_SIMPLE = 5
NUM_BUCKETS=10

UPPER_BOUNDS = [env.observation_space.high[0], 0.5, env.observation_space.high[2], np.math.radians(50)/1.]
LOWER_BOUNDS = [env.observation_space.low[0], -0.5, env.observation_space.low[2], -np.math.radians(50)/1.]
test_state= [-6,-0.3, 4, 0.7] # look later at how our state is defined if it is (,.,.,.) or [,.,.,.,.,]
#pos, velocity, angle, angular velocity
test_state_new= (0.19, 0.4, -1.3, 3.5)
LOWER_BOUNDS_SIMPLE=[0,0,0,0]
UPPER_BOUNDS_SIMPLE=[1,1,1,1]
#just to test


def round_partial(value, resolution):
    '''
    :param value: value we want to round
    :param resolution: at which steps we want to round (ex. 0.1 accuracy or .25 accuracy,..)
    :return: rounded value
    '''
    return round(value/resolution)* resolution
#print(round_partial(10.38, 0.25))
#this should print 10.5 (value 10.38 rounded to resolution 0.25)
#print(round_partial(1.2, 0.5))
# should evaluate to 1.0 (rounded to next 0.5 accuracy)



def tuple_to_scalar(state):
    '''
    method that transforms a 4tuple input into a scalar
    :param state: 4tuple state as input, gets transformed to a scalar
    :return: scalar state
    '''
    scalar_state= 1000*state[0]+100*state[1]+10*state[2] +1*state[3]
    tuple= [scalar_state/1000, scalar_state%1000, scalar_state%100, scalar_state%10] # this maps back scalars to tuplestate

    return scalar_state








def discretize(state,lower_bounds= LOWER_BOUNDS, upper_bounds= UPPER_BOUNDS, num_buckets=NUM_BUCKETS):
    '''
    :param state: 4 tuple of (pos, velocity, angle, angular velocity)
    :param buckets: steps we do between lower and upperbound, stepsize
    :param lower_bounds: lower bound of our variables
    :param upper_bounds: upper bound of variables
    :return: tuple of discretized states
    '''
    print('upper bounds are {}'.format(upper_bounds))
    print('lower bounds are {}'.format(lower_bounds))
    #print('state is {}'.format(state))
    state= test_state       #state: [-6,-0.3, 4, 0.7] --> should be in buckets [0, 2, 9,9]
    print('state is {}'.format(state))
    stepsize= [(upper_bounds[i]-lower_bounds[i])/num_buckets for i in range(len(state))]
    num_states = [(upper_bounds[i] - lower_bounds[i]) / stepsize[i] for i in range(len(state))]  #10 for every tuple
    #print('numstates is {}'.format(num_states))
    for i in range(4):
        if state[i]<lower_bounds[i]:
         state[i]= lower_bounds[i]
        if state[i]>upper_bounds[i]:
         state[i]= upper_bounds[i]

    bucketindex= [(np.floor((state[i]-lower_bounds[i]) / stepsize[i])) for i in range(len(state))] #TODO changed --> .lower_bounds[i] gives neg values
    #bucketindex= [(np.floor(state[i]/stepsize[i])) for i in range(len(state))]
    for i in range(4):
        if bucketindex[i]==NUM_BUCKETS: #needed so that we do only have bucket 0..9
            bucketindex[i]-=1

    print('bucketindex is {}'.format(bucketindex))
    #new_state= [min(upper_bounds[i], (bucketindex[i]+0.5)*stepsize[i]) for i in range(len(state))]
    #new_state= [max(new_state[i], lower_bounds[i])for i in range(len(state))]
    #new_state= [(bucketindex[i]+ lower_bounds[i]+(stepsize[i]/2))*stepsize[i] for i in range(len(state))]
    #TODO: comment above in to have the old solution
    new_state= [round_partial((bucketindex[i]*stepsize[i]), stepsize[i]) for i in range(len(state))]
    new_state= [new_state[i]+ lower_bounds[i]+ (stepsize[i]/2) for i in range(len(state))]
    return tuple(new_state)
print('discretized state is {}'.format(discretize(test_state)))

def discretize_index(state,lower_bounds= LOWER_BOUNDS, upper_bounds= UPPER_BOUNDS, num_buckets=NUM_BUCKETS):
    '''
    :param state: 4 tuple of (pos, velocity, angle, angular velocity)
    :param buckets: steps we do between lower and upperbound, stepsize
    :param lower_bounds: lower bound of our variables
    :param upper_bounds: upper bound of variables
    :return: tuple of discretized states
    '''

    #print('state is {}'.format(state))

    stepsize= [(upper_bounds[i]-lower_bounds[i])/num_buckets for i in range(len(state))]
    for i in range(4):
        if state[i]<lower_bounds[i]:
         state[i]= lower_bounds[i]
        if state[i]>upper_bounds[i]:
         state[i]= upper_bounds[i]

    num_states = [(upper_bounds[i] - lower_bounds[i]) / stepsize[i] for i in range(len(state))]  #10 for every tuple
    #print('numstates is {}'.format(num_states))
    #bucketindex= [(np.floor((state[i]-lower_bounds[i]) / stepsize[i])) for i in range(len(state))] #TODO: changed s.t. not neg
    bucketindex = [(np.floor(state[i]- lower_bounds[i] / stepsize[i])) for i in range(len(state))] #TODO changed added lower bound
    print('bucketindex in indexfunctionn is {}'.format(bucketindex))
    new_state= [min(upper_bounds[i], (bucketindex[i]+0.5)*stepsize[i]) for i in range(len(state))]
    new_state= [max(new_state[i], lower_bounds[i])for i in range(len(state))]
    #this new_state is implemented so that it is never above upper bounds and never below lower bounds
    #if it would be we map it to upper respectively lower bounds
    state_index= bucketindex
    state_index= int(round(tuple_to_scalar(state_index)))
    return state_index
#print( 'state index of state is {}'.format(discretize_index(test_state)))


















