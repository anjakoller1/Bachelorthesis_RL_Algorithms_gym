import gym
import time
import numpy as np

MAX_ITERATIONS=1000
THETA= 1e-20

def run__episode(env, policy, gamma=1.0, render=False):
    total_reward = 0
    obs = env.reset()

    #  runs one episode, evaluates a policy by using it to run an episode
    #  and find its total reward
    while True:
        if render:
            env.render()

        obs, reward, done, info = env.step(int(policy[obs]))
        # we take a step according to the policy for our env now
        total_reward += (gamma * reward)
        if done:
            break
    return total_reward


# env= gym.make('FrozenLake8x8-v0').env

def policy_evaluation(env, policy, gamma=1.0, n_episodes=100):
    # evaluates a policy, runs it n_episodes times and returns average total reward
    scores = [run__episode(env, policy, gamma=gamma)
              for episode in range(n_episodes)]

    return np.mean(scores) # returns average of totalrewards over episodes
    #return np.max(scores)
    # this is not 20 as you get 20 points for successful dropof but -1 for every
    # timestep it takes (-10 for illegal pickup, dropoff)
    #if we get a max of 15 means --> fastest dropoff in 5 steps

def value_iteration(env, gamma=1.0, maxiterations=MAX_ITERATIONS, theta=THETA):
    V = np.zeros(env.nS)  # initialize V arbitrarily (V(s)=0 for all s)
    delta = 0
    for i in range(maxiterations):
        v = np.copy(V)  # store previous value function
        for state in range(env.nS):  # for every state do the value iteration step (page 67 script)
            q_sa = [sum([p * (r + v[s_new]) for p, s_new, r, done in env.P[state][a]]) for a in range(env.nA)]
            #  this computes update step (Bellman eq, page 67)
            V[state] = max(q_sa)
            delta = np.sum(np.abs(v - V))
            # delta= max(delta,np.abs(v-V).any())
            # we need the any so that the boolean is true if all elements are true
        if (delta <= theta):  # when V converges

            # if(max(delta,np.abs(v-V))<convergence_boundary):
            print('Value iteration converged at iteration number {}'.format(i + 1))
            time.sleep(5) #stops for 5s, just to see when it converges
           # n_iterations_until_convergence = i + 1
        else:
            print('No convergence until {} iterations'.format(maxiterations))
            # to get convergence choose a higher convergence_boundary 
    return V


def policy_extraction(v, gamma=1.0):  # policy improvement
    policy = np.zeros(env.nS)  # extract policy given a value function
    for state in range(env.nS):
        q_sa = np.zeros(env.action_space.n)
        for action in range(env.action_space.n):  # for all actions
            for nextprob in env.P[state][action]:
                # env.P are transition probabilities for the environment
                p, s_new, r, done = nextprob
                # nextprob is (probability, next state, reward, done)
                q_sa[action] += (p * (r + gamma * v[s_new]))
        policy[state] = np.argmax(q_sa)
    return policy


# this is the output step of the valueiteration of page 67


if __name__ == '__main__':
    env = gym.make('Taxi-v2').env
    #env=gym.make('FrozenLake-v0').env
    #env = gym.make('Taxi-v2').env
    # play with different environments
    env.reset()
    env.render()  # to see starting state of the environment
    starttime = time.time()
    optimal_value = value_iteration(env, gamma=0.6)
    # computes the optimal value function according to the value iteration algorithm
    policy = policy_extraction(optimal_value, gamma=0.6)
    # computes the optimal policy according to the optimal value function
    policy_score = policy_evaluation(env, policy, gamma=0.6, n_episodes=1000)
    # evaluates with the optimal policy
    endtime = time.time()
    total_time = endtime - starttime

    env.render()  # to see end state of the environment
    print('Total time is {}'.format(total_time))
    print('average score (reward) is {}'.format(policy_score))
