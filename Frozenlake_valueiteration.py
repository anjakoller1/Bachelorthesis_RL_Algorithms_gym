import gym
import numpy as np
import time
from pprint import pprint
'''
FrozenLake environment (8x8): 
actions: move up, down, left , right
states: Starting point, F: frozen surface (safe), H: Hole (not safe), G: goal 
8x8= 64 possible state 
reward +1: if goal reached, 0: otherwise 
'''

'''
Valueiteration alg: 1 sweep policy eval and one sweep policy improvement
to get faster convergence do multiple policy eval sweeps between each policy improvement
algorithm: s: set of state, a: set of actions, p(s'|s,a): state transition model
describes how the environment state changes when user performs action a depending on action a and current state s
p(r'|s,a): reward model: describes reward-value that the agent receives from the environment
after performing an action
gamma: discountfactor: controls importance of future rewards 
policy: the way by which the agent chooses which action to perform 
(function that takes curent state and returns an action pi(s):S->A)
value function V(s): how good is a state for an agent to be in 
(=expected total reward starting from state s)
rewards added with discountfactor gamma s.t. near rewards have higher weight
goal of agent: pick best policy that maximizes total rewards
optimal value func= maximum of value functions 
optimal policy= policy that corresponds to optimal value function 
Q-function: state action pair, returns real value (Q(s,a)= expected total reward received
by agent starting in s and picks action a and then behaves optimally) 
--> optimal value: maximum qfunction (indication how good it is to pick action a in state s) 
--> policy is action that gives maximum Q(s,a) for s
Valueiteration: computes optimal state value function by iteratively improving the estimate of V(s)
V(s) initialized randomly, repeatedly Q(s,a) and V(s) get updated until convergence 
'''
MAX_ITERATIONS = 1000 # maximum number of iterations
THETA = 0.1 # convergence boundary


def run__episode(env, policy, gamma=1.0, render=False):
    '''
    runs one episode and evaluates a policy by using it to run an episode and finds its total reward
    :param env:environment
    :param policy: policy with which we perform the episode run
    :param gamma:discountfactor
    :param render: if this is true we render the best action to perform for every state
    :return:total_reward
    '''
    total_reward = 0
    obs = env.reset()
    done=False
    while not done:
        if render:
            env.render()
        obs, reward, done, info = env.step(int(policy[obs]))
        # we take a step according to the policy for our env now
        total_reward += (gamma * reward)
    return total_reward


def policy_evaluation(env, policy, gamma=1.0, n_episodes=100):
    '''

    :param env:environment
    :param policy:policy
    :param gamma:discountfacot
    :param n_episodes: number of episodes we iterate over
    :return: mean of the total rewards (mean score) over episodes
    '''
    # evaluates a policy, runs it n_episodes times and returns average total reward
    scores = [run__episode(env, policy, gamma=gamma)
              for episode in range(n_episodes)]

    return np.mean(scores) # returns average of totalrewards over episodes

    # this is not 20 as you get 20 points for successful dropof but -1 for every
    # timestep it takes (-10 for illegal pickup, dropoff)
    # if we get a max of 15 means --> fastest dropoff in 5 steps

def value_iteration(env, gamma=1.0, maxiteration= MAX_ITERATIONS, theta=THETA):
    '''

    :param env:environment
    :param gamma: discount factor
    :param maxiteration: maximum number of iterations
    :param theta:convergence_boundary parameter set to THETA
    :return: Value function V
    '''
    V = np.zeros(env.nS)  # initialize V arbitrarily (V(s)=0 for all s)
    delta = 0
    for i in range(maxiteration):
        v = np.copy(V)  # store previous value function
        for state in range(env.nS):  # for every state do the value iteration step (page 67 script)
            q_sa = [sum([p * (r + v[s_new]) for p, s_new, r, done in env.P[state][a]]) for a in range(env.nA)]
            #  this computes update step (Bellman eq, page 67)
            V[state] = max(q_sa)
            delta = np.sum(np.abs(v - V))
            # delta= max(delta,np.abs(v-V).all())
        #if (delta <= theta):  # when V converges
        if(np.all(abs(v-V)<=theta)):

          # TODO
            convergence_rounds= i+1
            print('Value iteration converged at iteration number {}'.format(i + 1))
            time.sleep(5) #stops for 5s, just to see when it converges
            break

    return V, convergence_rounds
        # TODO: additionally return  convergence_rounds


def policy_extraction(env, v, gamma=1.0):
    '''

    :param v: value function
    :param gamma: discountfactor
    :return: policy
    '''
    policy = np.zeros(env.nS)  # extract policy given a value function
    for state in range(env.nS):
        q_sa = np.zeros(env.action_space.n)
        for action in range(env.action_space.n):  # for all actions
            for nextprob in env.P[state][action]:
                # env.P are transition probabilities for the environment
                p, s_new, r, done = nextprob
                # nextprob is (probability, next state, reward, done)
                q_sa[action] += (p * (r + gamma * v[s_new]))
        policy[state] = np.argmax(q_sa)
    return policy


# this is the output step of the valueiteration of page 67
def env_test(env_name, gamma, theta = THETA, max_iterations = MAX_ITERATIONS):
    '''

    :param env_name: environment name e.g. 'Taxi-v2', 'FrozenLake-v0'
    :param gamma: discountfactor
    :param theta: convergence_boundary parameter
    :param max_iterations: maximum number of iterations
    :return: convergence_rounds, (number of rounds it needs until convergence),
    mean_score (total reward), total_time
    '''
    print('Running %s with gamma=%s' % (env_name, gamma))
    print('Initial state:')
    env = gym.make(env_name).env
    env.reset()
    env.render()
    start_time = time.time()
    #optimal_value= value_iteration(env, gamma=gamma)
    # TODO
    optimal_value, convergence_rounds = value_iteration(env, gamma=gamma)
    # computes the optimal value function according to the value iteration algorithm
    policy = policy_extraction(env, optimal_value, gamma=gamma)
    # computes the optimal policy according to the optimal value function
    mean_score = policy_evaluation(env, policy, gamma=gamma, n_episodes=1000)
    # evaluates with the optimal policy
    end_time = time.time()
    total_time = end_time - start_time
    print('Total time is {}'.format(total_time))
    print('average score (reward) is {}'.format(mean_score))
    #TODO
    print('Converges after {} rounds'.format(convergence_rounds))
    print('Final state: ')
    env.render()
    print(value_iteration(env, gamma=gamma))
    return mean_score, total_time, convergence_rounds
    #TODO
    # as well return convergence_rounds


if __name__ == '__main__':
    ''''
    env = gym.make('FrozenLake8x8-v0').env
    #env = gym.make('Taxi-v2').env
    env.reset()
    env.render()  # to see starting state of the environment
    gamma = 1.0  # discountfactor
    start_time = time.time()
    optimal_value, convergence_rounds = value_iteration(env, gamma=0.6)
    # computes the optimal value function according to the value iteration algorithm
    policy = policy_extraction(optimal_value, gamma=0.6)
    # computes the optimal policy according to the optimal value function
    policy_score = policy_evaluation(env, policy, gamma=0.6, n_episodes=1000)
    # evaluates with the optimal policy
    end_time = time.time()
    total_time = end_time - start_time

    env.render()  # to see end state of the environment
    print('Total time is {}'.format(total_time))
    print('average score (reward) is {}'.format(policy_score))
    
    '''
    env_name = 'FrozenLake-v0'
    env_names=['Taxi-v2', 'FrozenLake8x8-v0', 'FrozenLake-v0']
    gammas= [ 0.5,0.6, 0.9, 1.0]
    statistics_dict = {}
    for gamma in gammas:
        # here as well return convergence_rounds
        mean_scoure,convergence_rounds, total_time = env_test(env_name=env_name, gamma=gamma )
        statistics_dict[gamma]=[
            #TODO
             'Convergence after rounds: {}'.format(convergence_rounds),
                                'Mean_score : {}'.format(mean_scoure),
                                'Total time: {}'.format(total_time)]
    print('Gamma results: ')
    pprint(statistics_dict)

