import gym
import time
import numpy as np
from pprint import pprint
import os
from cartpole_new import CartPoleNewEnv

MAX_ITERATIONS = 100000  # maximum number of iterations
THETA = 0.1  # convergence boundary
SEP = ';'
PATH = '/home/anjak/Dokumente'
env= CartPoleNewEnv()
obs= env.reset()
OBSDIM= len(obs)
NUM_ACTIONS= env.action_space.n
NUM_BUCKETS= 30 #TODO play with this value
NUM_STATES= pow(NUM_BUCKETS,OBSDIM-1)
upperbounds= env.upper_bounds
lower_bounds= env.lower_bounds

def write_csv(policy, file_name):
    '''

    :param file_name: filename that we want to save the data in
    :return:
    '''
    # file_name= 'valueiteration_cartpole_policy.csv'
    outpath = os.path.join(PATH, file_name)
    np.savetxt(outpath, policy, delimiter=SEP)


def read_csv(inpath):
    # inpath = '/home/anjak/Dokumente/qlearningmatrix.csv'
    matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
    return matrix

def run__episode(env, policy, gamma=1.0, render=False):
    '''
    runs one episode and evaluates a policy by using it to run an episode and finds its total reward
    :param env:environment
    :param policy: policy with which we perform the episode run
    :param gamma:discountfactor
    :param render: if this is true we render the best action to perform for every state
    :return:total_reward
    '''
    total_reward = 0
    obs = env.reset()
    newobs,obsindex= env.discretize(obs)
    obsindex= env.tuple_to_scalar()
    done = False
    steps=0
    while not done:
        if render:
            env.render()
        action_to_take= int(round(policy[obsindex]))
        obs, reward, done, info = env.step(action_to_take)
        # we take a step according to the policy for our env now
        if done:
            env.reset()
            break
        new_state, new_state_index= env.discretize(new_state)
        total_reward += reward
        steps+=1
        if (total_reward>195):
            print('you won the game')
    return total_reward


def policy_evaluation(env, policy, gamma, n_episodes=100):
    '''

    :param env:environment
    :param policy:policy
    :param gamma:discountfacot
    :param n_episodes: number of episodes we iterate over
    :return: mean of the total rewards (mean score) over episodes
    '''
    # evaluates a policy, runs it n_episodes times and returns average total reward
    scores = [run__episode(env, policy, gamma=gamma) for episode in range(n_episodes)]

    return np.mean(scores)  # returns average of totalrewards over episodes


def value_iteration(env, gamma=1.0, maxiteration=MAX_ITERATIONS, theta=THETA, num_states= NUM_STATES, num_actions= NUM_ACTIONS):
    '''

    :param env:environment
    :param gamma: discount factor
    :param maxiteration: maximum number of iterations
    :param theta:convergence_boundary parameter set to THETA
    :return: Value function V
    '''
    #TODO test the states and test env.states --> correspondence
    convergence_rounds=0
    policy = np.zeros(num_states)
    V = np.zeros(num_states, dtype=np.float)  # initialize V arbitrarily (V(s)=0 for all s)

    for i in range(maxiteration):
        delta=0
        temp_update= np.zeros(num_actions)

        for state in range(num_states):  # for every state do the value iteration step (page 67 script)
            v = np.copy(V[state])  # store previous value function
            old_state= state

            for action in range(num_actions):
                state= env.set_state(old_state) #this sets state back to state before the action taken
                probability= 1
                new_state, reward, done, info= env.step(action)
                if done:
                    env.reset()
                    break
                new_state_discretized, new_state_discretized_index= env.discretize(new_state)
                new_state_discretized_index= int(np.floor(env.tuple_to_scalar()))
                temp_update[action]= probability*(reward+ (gamma * V[new_state_discretized_index]))

            V[state]= max(temp_update)
            print('V[state]: {}'.format(V[state]))
            delta= max(delta,np.abs(v-V[state]))
        if (delta <= theta):  # when V converges
            break
        convergence_rounds = i+1
    #TODO here save policy and return it
    policy[state]= np.argmax(temp_update)
    print('policy[state]: {}'.format(policy[state]))
    print('Value function is {}'.format(V))

    return V, convergence_rounds,policy
    # TODO: additionally return  convergence_rounds


def policy_extraction(env, V, gamma, num_states= NUM_STATES, num_actions= NUM_ACTIONS):
    '''

    :param v: value function
    :param gamma: discountfactor
    :return: policy
    '''
    policy = np.zeros(num_states)  # extract policy given a value function
    for state in range(num_states):
        q_sa = np.zeros(num_actions)
        old_state= state
        for action in range(num_actions):  # for all actions
            state= env.set_state(old_state)
            p=1.0
            new_state, reward, done, info= env.step(action)
            if done:
                env.reset()
                break
            new_state_discr, new_state_dicr_index= env.discretize(new_state)
            new_state_dicr_index= int(env.tuple_to_scalar())
            q_sa[state]= p*(reward + gamma * V[new_state_dicr_index])
        policy[state] = np.argmax(q_sa)
    print('Policy is {}'.format(policy))
    return policy



# this is the output step of the valueiteration of page 67
def env_test(env, gamma, theta=THETA, max_iterations=MAX_ITERATIONS):
    '''

    :param env_name: environment name e.g. 'Taxi-v2', 'FrozenLake-v0'
    :param gamma: discountfactor
    :param theta: convergence_boundary parameter
    :param max_iterations: maximum number of iterations
    :return: convergence_rounds, (number of rounds it needs until convergence),
    mean_score (total reward), total_time
    '''
    env.reset()
    print('Running with gamma=%s' % (gamma))
    start_time = time.time()
    #optimal_value = value_iteration(env, gamma=gamma)
    # TODO
    optimal_value, convergence_rounds, policy = value_iteration(env, gamma=gamma)
    # computes the optimal value function according to the value iteration algorithm
    #policy= policy_extraction(env, optimal_value, gamma=gamma)
    write_csv(policy, 'valueiteration_policy_cartpole_30b.csv')
    write_csv(optimal_value, 'valueiteration_valuefunction_cartpole_30b.csv')
    # computes the optimal policy according to the optimal value function
    print('optimal policy is {}'.format(policy))
    #mean_score = policy_evaluation(env, policy, gamma=gamma, n_episodes=100000)
    #TODO: comment above line out if you want the meanscore over the episodes
    # evaluates with the optimal policy
    end_time = time.time()
    total_time = end_time - start_time
    print('Total time is {}'.format(total_time))
    #print('average score (reward) is {}'.format(mean_score))
    print('Converges after {} rounds'.format(convergence_rounds))
    return total_time, convergence_rounds, policy



if __name__ == '__main__':
    '''
    here we call our test algorithm with different gamma values and give the parameters for the function swe call 
    '''
    env= CartPoleNewEnv()
    #gammas = [ 0.5, 0.6, 0.7, 0.8, 0.9]
    gammas= [0.9] #faster for testing and debugging
    statistics_dict = {}
    #i=0
    #convergence_rounds_over_10_it=0
    #convergence=[]
    #for i in range(9):

    for gamma in gammas:
            # here as well return convergence_rounds
            total_time, convergence_rounds, policy = env_test(env, gamma=gamma)
     #       convergence.append(convergence_rounds)
      #      convergence_rounds_over_10_it = np.mean(convergence)
       #     i = i + 1
            statistics_dict[gamma] = [
                'Convergence rounds: {}'.format(convergence_rounds),
                #'Mean_score : {}'.format(mean_score),
                'Total time: {}'.format(total_time),
                'Policy: {}'.format(policy)]
    print('Gamma results: ')
    pprint(statistics_dict)



