import time
from pprint import pprint
import gym
import numpy as np
import os
from cartpole_new import CartPoleNewEnv

MAX_ITERATIONS = 100000  # maximum number of iterations
THETA = 0.1  # convergence boundary
SEP = ';'
PATH = '/home/anjak/Dokumente'

'''
pos, velocity, angle, angular velocity
lowerbound: [-4,8, -0.5, -0.418 (radians), -50(deg)]
upperbound: [4.8, 0.5, 0.418, 50]
episode termination if pos > +-2.4, angle >+-24, or episode_length >200 --> then the variable done is set to true
test_state= [-6, 0.78, 5, 0.34]
'''
class CartPolevalueit(object):
    def __init__(self):
        self.env= CartPoleNewEnv()
        self.state = self.env.reset()
        self.new_state, self.new_state_index = self.env.discretize(self.env.state)
        self.obs_dim= np.size(self.env.state)
        self.num_buckets= self.env.num_buckets
        self.num_actions= self.env.action_space.n
        self.num_states= pow(self.num_buckets, self.obs_dim -1)
        self.upper_bounds= self.env.upper_bounds
        self.lower_bounds= self.env.lower_bounds



    def multiplesteps_in_actiondir(self,action, num_actionsteps=3):
        
        #this should perform 5 times the action that was choosen
        #:param action:action that we want to perform multiple times
        #:param num_actionsteps: number of steps with this action we take
        #:return: state , reward, done, info
        
        for i in range(num_actionsteps):
            self.env.state, reward, done, info= self.env.step(action)
        return self.env.state, reward, done, info


    def write_csv(self,policy, file_name):
        '''

        :param file_name: filename that we want to save the data in
        :return:
        '''
        outpath = os.path.join(PATH, file_name)
        #np.savetxt(outpath, policy, delimiter=SEP)

    def read_csv(inpath):
        # inpath = '/home/anjak/Dokumente/qlearningmatrix.csv'
        matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
        return matrix


    def scalar_to_tuple(self,scalar):
        '''
        takes a scalar state and maps it to a 4tuplestate
        :param scalar: input we want to have in a 4tuple
        :return: 4vector of the input
        '''
        self.env.state= [(scalar/1000),((scalar % 1000)/100), ((scalar % 100)/10), ((scalar % 10)/1)]
        return self.env.state

    def run__episode(self, policy, V, Q, T, render=False):
        '''
        runs one episode and evaluates a policy by using it to run an episode and finds its total reward
        :param env:environment
        :param policy: policy with which we perform the episode run
        :param gamma:discountfactor
        :param render: if this is true we render the best action to perform for every state
        :return:total_reward
        '''
        total_reward = 0
        self.new_state = self.env.reset()
        self.env.state_discretized, self.env.state_discretized_index = self.env.discretize(self.new_state)
        self.env.state_discretized_index = int(np.floor(self.env.tuple_to_scalar()))
        self.env.set_state(self.env.state_discretized_index)
        obsindex = self.env.state_discretized_index  # TODO here change this as self.env.state is not discretized
        done = self.env.is_done()
        steps = 0

        while not done:
            if render:
                self.env.render()
            action_to_take = int(round(policy[obsindex]))
            print('S: {}, V: {}, Q: {}, P: {}.'.format(obsindex, V[obsindex], Q[obsindex], policy[obsindex]))
            # TODO take multiple steps in dir of actino
            # self.env.state, reward, done, info= self.env.multiplesteps_in_actiondir(action_to_take, 1)
            self.new_state, reward, done, info = self.env.step(action_to_take)

            self.env.state_discretized, self.env.state_discretized_index = self.env.discretize(self.new_state)
            self.env.state_discretized_index = int(np.floor(self.env.tuple_to_scalar()))

            newindex = self.env.state_discretized_index

            if T[obsindex][action_to_take] > -1:
                assert (T[obsindex][action_to_take] == newindex)
            else:
                T[obsindex][action_to_take] = newindex

            obsindex = newindex

            if done:
                assert (self.env.is_done())
                break

            self.env.set_state(obsindex)

            # total_reward += (gamma * reward) #TODO: just summed up reward or discounted?
            total_reward += reward
            steps += 1
            if (total_reward >= 195):
                print('you won the game')
                return
        # print('number of steps per episode {}'.format(steps))
        return total_reward

    def policy_evaluation(self, policy,V,Q,T, gamma, n_episodes=100):
        '''
        to win we should have score 195 in 100 consecutive episodess and returns average total reward
        evaluates a policy runs it n_episodes time
        :param env:environment
        :param policy:policy
        :param gamma:discountfacot
        :param n_episodes: number of episodes we iterate over
        :return: mean of the total rewards (mean score) over episodes
        '''
        scores = [self.run__episode(policy,V,Q,T, True) for episode in range(n_episodes)]
        return np.mean(scores)

    def value_iteration(self, gamma, maxiteration=MAX_ITERATIONS, theta=THETA):
        '''

        :param env:environment
        :param gamma: discount factor
        :param maxiteration: maximum number of iterations
        :param theta:convergence_boundary parameter set to THETA
        :return: Value function V
        '''
        convergence_rounds = 0
        policy = np.zeros(self.num_states)
        V = np.zeros(self.num_states, dtype=float)
        Q = np.zeros((self.num_states, self.num_actions), dtype=float)
        T = np.negative(np.ones((self.num_states, self.num_actions), dtype=int))

        for i in range(maxiteration):
            delta = 0
            temp_update = np.zeros(self.num_actions)

            for s in range(
                    self.num_states):  # TODO: for every state in STATES (redefine env.nS) and reset state in the env impl
                v = np.copy(V[s])  # store previous value function
                # self.new_state_index= s
                old_state = s
                # TODO check if done= True --> if yes skip action loop and leave valuefunction going to 0
                # TODO: modify the check for being done so that does not take into account the xvalue
                self.env.set_state(old_state)
                if self.env.is_done():
                    continue  # this skips the iteration if done= true but continues with the next iteratoin

                for action in range(self.num_actions):
                    # s= self.env.set_state(old_state)
                    self.env.set_state(old_state)
                    # self.state = s
                    probability = 1.0
                    # self.env.state_discretized, reward, done, info= cartpoleobj.multiplesteps_in_actiondir(action,num_actionsteps=1)
                    self.new_state, reward, done, info = self.env.step(action)

                    self.env.state_discretized, self.env.state_discretized_index = self.env.discretize(self.new_state)
                    self.env.state_discretized_index = int(np.floor(self.env.tuple_to_scalar()))
                    temp = V[self.env.state_discretized_index]
                    temp_update[action] = probability * (reward + (gamma * temp))
                    # temp_update[action] = probability*(reward +(gamma * V[new_state_index])) #sum up for all newstates and rewards
                    Q[s][action] = temp_update[action]

                    if T[old_state][action] > -1:
                        assert (T[old_state][action] == self.env.state_discretized_index)
                    else:
                        T[old_state][action] = self.env.state_discretized_index

                    if self.env.is_done():  # TODO changed here
                        assert (done)
                        self.env.reset()
                        break

                policy[s] = np.argmax(temp_update)
                V[s] = max(temp_update)  # max(q_sa)
                delta = max(delta, np.abs(v - V[s]))
                i += 1

            if (delta <= theta):  # when V converges
                temp1 = V[s]  # to debug only
                print('V[state] is {}'.format(temp1))
                print('v is {}'.format(v))
                print('valueiteration converged')
                convergence_rounds = i + 1
                # print('Value iteration converged at iteration number {}'.format(i + 1))
                break
            convergence_rounds = i + 1
        # print('policy[{}]: {}'.format(s,policy[s]))
        # print('Value function is {}'.format(V))

        return V, convergence_rounds, policy, Q, T

    def policy_extraction( self,V,env, gamma=1.0):
        '''
        this method should extract the policy given a value function
        :param v: value function
        :param gamma: discountfactor
        :return: policy
        '''
        policy = np.zeros(self.num_states)  # extract policy given a value function
        for state in range(self.num_states):
            q_sa = np.zeros(self.num_actions)
            old_state= state
            for action in range(self.num_actions):  # for all actions
                state= self.env.set_state(old_state)
                p=1.0
                self.env.state, reward, done, info= self.env.step(action)
                if done:
                    self.env.reset()
                    break
                self.env.new_state, self.env.bucketindex= self.env.discretize(self.env.state)
                self.env.bucketindex= int(self.env.tuple_to_scalar()) #todo: delete self.env.bucketindex
                temp= V[self.env.bucketindex] #to debug
               # q_sa[action] = (p * (reward + gamma * temp))
                q_sa[state]= p* (reward + gamma * temp)
                #q_sa[action]= (p*(reward+ gamma*V[newstateindex]))
            policy[state]= np.argmax(q_sa)
        print('policy is {}'.format(policy))
        return policy


    def env_test(self, gamma):
        '''

        :param env_name: environment name e.g. 'Taxi-v2', 'FrozenLake-v0'
        :param gamma: discountfactor
        :param theta: convergence_boundary parameter
        :param max_iterations: maximum number of iterations
        :return: convergence_rounds, (number of rounds it needs until convergence),
        mean_score (total reward), total_time
        '''
        print('Running %s with gamma=%s' % (self.env, gamma))
        start_time = time.time()
        optimal_value, convergence_rounds, policy, Q, T = self.value_iteration(gamma=gamma) #TODO
        self.write_csv(policy,'valueiteration_cartpole_policy_30buckets.csv')
        self.write_csv(optimal_value, 'valueiteration_cartpole_valuefunction_30buckets.csv')
        print('optimal policy is {}'.format(policy))
        mean_score = self.policy_evaluation(policy,optimal_value,Q,T, gamma=gamma, n_episodes=1000) #TODO
        # evaluates with the optimal policy
        end_time = time.time()
        total_time = end_time - start_time
        print('Total time {}'.format(total_time))
        print('average score (reward) {}'.format(mean_score))
        print('Converges after {} rounds'.format(convergence_rounds))
        return mean_score, total_time, convergence_rounds, policy


if __name__ == '__main__':
    '''
    here we call our test algorithm with different gamma values and give the parameters for the functions we call 
    '''
    cartpoleobj= CartPolevalueit()
    #gammas = [0.1, 0.3, 0.4, 0.6, 0.8, 0.9]
    gammas = [0.9] #TODO comment out above for full test, this here for easier testing
    statistics_dict = {}
    i=0
    convergence_rounds_over_10_it=0
    convergence=[]

    for gamma in gammas:
        mean_score, total_time, convergence_rounds, policy = cartpoleobj.env_test(gamma=gamma)
        statistics_dict[gamma] = [
            'Convergence rounds: {}'.format(convergence_rounds),
            'Mean_score : {}'.format(mean_score),
            'Total time: {}'.format(total_time),
            'Policy is : {}'.format(policy)]


    '''
    for i in range(9):

        for gamma in gammas:
            mean_score, total_time, convergence_rounds, policy = env_test(env=env, gamma=gamma)
            convergence.append(convergence_rounds)
            convergence_rounds_over_10_it = np.mean(convergence)
            i = i + 1
            statistics_dict[gamma] = [
                'Convergence rounds: {}'.format(convergence_rounds_over_10_it),
                'Mean_score : {}'.format(mean_score),
                'Total time: {}'.format(total_time),
                'Policy is : {}'.format(policy)]
    '''
    print('Gamma results: ')
    pprint(statistics_dict)



