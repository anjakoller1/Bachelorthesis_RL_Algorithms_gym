import random
import gym
import numpy as np
import time
from pprint import pprint
import sys
import copy
import os
from cartpole_new import CartPoleNewEnv


# Params
MIN_EPSILON = 0.1
ALPHA = 0.7  # learning_rate
GAMMA = 0.6  # discount_factor
EPSILON = 0.5  # default epsilon value (with this probability we take a random action)
env = CartPoleNewEnv()
observation = env.reset()
NUM_ACTIONS = env.action_space.n
OBS_DIM = np.size(observation) #4
NUM_BUCKETS = 50   #TODO: play with different num_buckets (e.g. 100, 20, 30,50)
NUM_STATES = pow(NUM_BUCKETS, OBS_DIM-1)  #10000 #TODO here OBS_DIM -1 instead of OBS_DIM because state is not looked at
SEP = ';'
PATH = '/home/anjak/Dokumente'

def multiplesteps_in_actiondir(action, num_actionsteps):
    '''
    this should perform 5 times the action that was choosen
    :param action:action that we want to perform multiple times
    :param num_actionsteps: number of steps with this action we take
    :return:
    '''
    for i in range(num_actionsteps):
        new_state, reward, done, info = env.step(action)
        if done:
            break
    return new_state, reward, done, info


def write_csv(matrix, file_name):
    #file_name= 'qlearningmatrixpolicyit.csv'
    outpath = os.path.join(PATH, file_name)
    np.savetxt(outpath, matrix,delimiter=SEP)


def read_csv(inpath):
    #inpath = '/home/anjak/Dokumente/qlearningmatrix.csv'
    matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
    return matrix


def adaptive_epsilon_get(episode, total_episodes):
    # return max(min_epsilon, min(1, 1.0 - np.math.log10((episode + 1) / number_of_states)))
    if (episode < (total_episodes/10)): #1e5/10 = 1e4
    #if (episode < 10000):
        return 1.0
    if (episode > total_episodes*0.9):#90000
    #if (episode > 90000):
        return 0.0
    return EPSILON

def tuple_to_scalar(state, num_buckets=NUM_BUCKETS):
    '''
    this tuple_to_scalar method works for all bucketsizes
    method that transforms a 4tuple input into a scalar
    :param state: 4tuple state as input, gets transformed to a scalar
    :return: scalar state

    '''
    potence= np.math.log(num_buckets, 10) #log with basis 10
    scalar_state= (100**potence)*state[0]+(10**potence)*state[1] +(1**potence)*state[2]
    return scalar_state


def qlearning_alg(env, total_episodes=int(1e6), alpha=ALPHA, gamma=GAMMA, epsilon=EPSILON,  num_action= NUM_ACTIONS, num_states= NUM_STATES, render=False):
    '''

    :param env:environment we act in
    :param total_episodes: total number of episodes we go through
    :param alpha:learning rate from qlearning alg
    :param gamma: discountfactor from qlearningalg
    :return:qtable, convergence_rounds
    '''
    convergence_rounds = 0
    Q_table= np.full((num_states, num_action), 0, dtype=float) #TODO here change precision of entries to 4 (float16)
    done=False
    total_reward = 0
    policy = np.zeros(NUM_STATES, dtype=int)
    delta = 0
    old_policies = []
    num_old_policies = 100
    not_zero_counter= 0 #to debug, counts number of entries of Q that are not 0 (meaning got updated)
    for episode in range(total_episodes):
       # sys.stdout.write("\rEpisode %d -> delta is %.2f and total reward is %.2f" % (episode, delta, total_reward))
        #sys.stdout.write('\r{}'.format( Q_table))
        #sys.stdout.write('\r{}'.format(policy))
        #sys.stdout.flush()
        state = env.reset()
        done = False
        total_reward = 0.0
        state, oldstate_index= env.discretize(state)
        oldstate_index= int(tuple_to_scalar(oldstate_index, num_buckets= NUM_BUCKETS))
        epsilon = adaptive_epsilon_get(episode, total_episodes)
        # TODO: comment above row epsilon=adaptive... out when you dont want an adaptive but a fixed epsilon
        delta = 0
        while not done:
            if (random.uniform(0, 1) > epsilon):
                action = np.argmax(Q_table[oldstate_index])
            else:
                action = env.action_space.sample()
            # env.render() # to see which steps it takes
            #TODO take action mutliple times (vary with this)
            new_state, reward, done, info = multiplesteps_in_actiondir(action, 1) #TODO play with that number
            if render:
                env.render() #this renders the env if the variable render is set to true
            total_reward += reward
            new_state, state_index= env.discretize(new_state)
            state_index= int(tuple_to_scalar(state_index, num_buckets=NUM_BUCKETS))

            if done:
                Q_table[state_index] = 0.0  # this is like resetting the Q_table
                if episode % 500 == 0:
                    print('We lost [{}]: {}'.format(state_index, total_reward))
                break
            elif total_reward > 194:
                Q_table[state_index] = 0.0
                print('We won')
                break

            Q_update = alpha * (reward + gamma * np.max(Q_table[state_index]) - Q_table[oldstate_index, action])
            temp= Q_table[oldstate_index,action]
            Q_table[state_index, action] = temp + Q_update

            if Q_table[state_index, action] > 194:
                print("error should not be above 194 {}".format(Q_table[state_index, action]))



            delta = max(delta, abs(Q_update))
            oldstate_index = state_index # here proper reset state= new_state


            convergence_rounds += 1

            policy[state_index] =  np.argmax(Q_table[state_index])
            #print('policy is {}'.format(policy))

            oldstate_index = state_index

            #TODO comment out below
            if done:
                old_policies.append(copy.deepcopy(policy))
                break
          #remove very oldest policy
        if len(old_policies) > num_old_policies:
            old_policies.remove(old_policies[0])

        # policy[state] = np.argmax(Q_table[state])
        # print('policy in episode {} is {}'.format(episode, policy))
        allEqual = len(old_policies) == num_old_policies
        for i in range(len(old_policies)):
            allEqual = allEqual and np.all(old_policies[i] == policy)

        if allEqual:
            break
            #TODO comment out above


    #print('policy for gamma {} is {}'.format(gamma, policy))
    print('\n')
    print('Qlearning converges after {} episodes and {} rounds'.format(episode, convergence_rounds))
    # TODO include stopping criteria when policy is goodenough
    print('Q-table is {}'.format(Q_table))
    # to debug only
    policy[oldstate_index] = np.argmax(Q_table[oldstate_index]) # keep track of policy over different episodes and compare
    V_qlearning= np.zeros(num_states)
    #TODO also print out the V function of the newstate and the max of the policy
    #TODO: and this two should converge to the same thing
    V_qlearning[state_index]= np.argmax(Q_table[state_index,action])
    print('V_qlearning[new_state] {}'.format(V_qlearning[state_index]))
    V_qlearning[state_index]= np.max(policy[state_index]) #TODO here
    print('V_qlearning[new_state] of policy; {}'.format(V_qlearning[state_index]))
    return convergence_rounds, total_reward, episode, policy,Q_table, not_zero_counter


def test_funtion(env, gamma, epsilon=EPSILON):
    '''
    :param env: environment we test
    :param gamma: discountfactor
    :param epsilon : probability at which we choose an action at random
    :return: convergence_rounds, mean_score, total_time
    I iterate 10 times (and take the average) over my whole test_function with different gammas each time as I want to see the behavior
    on average and make sure that the values were not just a single coincidence.
    '''
    print('Running %s with gamma = %s' % (env, gamma))
    start_time = time.time()
    convergence_rounds, total_reward, episode, policy,q_table, not_zero_counter = qlearning_alg(env, gamma=gamma)
    write_csv(q_table,file_name='ql_cartpole_qtable_50b_1e7ep.csv')
    write_csv(policy, file_name='ql_cartpole_policy_50b_1e7ep.csv')
    #write_csv(policy, file_name='ql_cartpole_policy_30b_1e6ep_degrees3_newrewardfunc.csv')
    total_time = time.time() - start_time
    print('Convergence after {} rounds'.format(convergence_rounds))
    print('Runtime of the alg {}'.format(total_time))
    print('Total rewards {}'.format(total_reward))

    # TODO print convergence_rounds meanscore as well
    return total_time, convergence_rounds, total_reward, episode, policy


if __name__ == '__main__':
    '''
    mainfunction, calls the test function with different parameters 

    '''
    # iterate 10 times over the test and take the mean of it
    i = 0  # iterator
    convergence_rounds_over_10_it = 0
    convergence = []
    episodes= []
   # for i in range(9):
    #env_name = 'FrozenLake-v0'
    env=CartPoleNewEnv()
    # gammas= [0.1]
    #gammas= [0.2, 0.4, 0.6, 0.7, 0.8, 0.9, 0.99]
    gammas = [0.9]
    statistics_dict = {}
    for gamma in gammas:
        total_time, convergence_rounds, total_reward, episode, policy = test_funtion(env=env, gamma=gamma)
        #  total_time, convergence_rounds, total_reward = test_function(env_name=env_name, gamma=gamma)
                        # time_over_10_it += total_time
                        # convergence_rounds_over_10_it += convergence_rounds
                        # reward_over_10_it += total_reward
                       # convergence.append(convergence_rounds)
                        #convergence_rounds_over_10_it = np.mean(convergence)
                        #episodes.append(episode)
                        #episodes_over_10_it= np.mean(episode)
                       # i = i + 1
                        # print(time_over_10_it, reward_over_10_it, convergence_rounds_over_10_it)
        statistics_dict[gamma] = ['converges after {} episodes: '.format(episode),
                                                  'Convergence rounds : {}'.format(convergence_rounds),
                                                  'Total Reward : {}'.format(total_reward),
                                                  'Total time:{} '.format(total_time),
                                                  'Policy: {}'.format(policy)]

    print('Gamma results: ')
    pprint(statistics_dict)