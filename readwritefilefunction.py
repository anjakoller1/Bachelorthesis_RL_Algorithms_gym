import numpy as np
import os
SEP = ';'
PATH = '/home/anjak/Dokumente'

file = open("tabletest.csv","w")
file.write("This is a test")
file.close()

#def write_policy_to_file

file= open("test.csv","w")
vector=[0,1,3,5,16]
for i in vector:
    file.write(str(i))
    #valueof to convert back
    file.write(",")
    #TODO here that only one entry of policy is written in one cell
file.close()

#TODO use this method to save the Qtable in a csv file
#TODO: as well as the policy

#qtable= np.array([0,1,2],[1,9,8],[4,5,7])
a = np.asarray([[1,2,3], [4,5,6], [7,8,9]])
file_name = 'foo.csv'
outpath = os.path.join(PATH, file_name)
print('writing matrix to {path}'.format(path=outpath))
np.savetxt(outpath, a, delimiter=SEP)
#TODO write a matrix in csv file

def write_csv(matrix):
    file_name= 'qlearningmatrix.csv'
    outpath = os.path.join(PATH,file_name)
    np.savetxt(outpath, matrix,delimiter=SEP)


def read_csv(inpath):
    matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
    return matrix

filepath = '/home/anjak/Dokumente/foo.csv'
m = read_csv(filepath)
print(m)

vectormatrix= [0,1,3,4,5]
write_csv(vectormatrix)
