import numpy as np
import os
SEP = ';'
PATH = '/home/anjak/Dokumente'

def write_csv(self, policy, file_name):
    '''

    :param file_name: filename that we want to save the data in
    :return:
    '''
    # file_name= 'valueiteration_cartpole_policy.csv'
    outpath = os.path.join(PATH, file_name)
    np.savetxt(outpath, policy, delimiter=SEP)


def read_csv(inpath):
    # inpath = '/home/anjak/Dokumente/qlearningmatrix.csv'
    matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
    return matrix

def comparepolicy(inpatha,inpathb):
    notequalcounter=0
    policya= read_csv(inpath=inpatha)
    policyb= read_csv(inpath=inpathb)
    lengthpolicy= len(policya) #should be same as len(policyb)
    for i in range(lengthpolicy):
        if policya[i] != policyb[i]:
            notequalcounter +=1
            print('policy[{}]:polit {},ql {}'.format(i,policyb[i],policya[i]))
    print('notequalcounter: {}'.format(notequalcounter))
    equality= notequalcounter / lengthpolicy
    return equality


inpathpi= '/home/anjak/Dokumente/policyiterationpolicy_30b_boundaryresetfunc.csv'
inpathql= '/home/anjak/Dokumente/ql_cartpole_policy_30b_1e6ep_new_differentreset.csv'

print(comparepolicy(inpathpi, inpathb=inpathql))
