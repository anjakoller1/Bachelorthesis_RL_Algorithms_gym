import numpy as np
import os
import gym
import time
#import cartpole_new
from cartpole_new import CartPoleNewEnv
SEP = ';'
PATH = '/home/anjak/Dokumente'
NUM_BUCKETS= 50 #TODO change here when changed in cartpole_new.py
OBS_DIM= 3
NUM_STATES= pow(NUM_BUCKETS, OBS_DIM)
temp= NUM_STATES

class GameCartPole(object):
    def __init__(self):
        self.env= CartPoleNewEnv()
        self.env.state = self.env.reset()
        self.env.new_state, self.env.bucketindex = self.env.discretize(self.env.state)

        #self.state = self.env.state
        #self.new_state, self.bucketindex = self.env.new_state, self.env.bucketindex

    @staticmethod
    def read_user_input():
        '''
        if user presses action a is left (0) or user presses action 1 (right)
        :param env: environment we want to render and step according to the entered action
        :return: action, rendering (the action we entered and a rendering of the cart according to this action
        '''

        #TODO: read in keyboard inputs (a and s) 
        user_action = input("enter action (a /s) ")

        actions_dict = {'a': 0,
                        's': 1
                        }
        action = actions_dict[user_action] if user_action in actions_dict else None
        return action

    def write_csv(self,policy, file_name):
        '''

        :param file_name: filename that we want to save the data in
        :return:
        '''
        #file_name= 'valueiteration_cartpole_policy.csv'
        outpath = os.path.join(PATH, file_name)
        np.savetxt(outpath, policy, delimiter=SEP)

    def read_csv(self,inpath):
        # inpath = '/home/anjak/Dokumente/qlearningmatrix.csv'
        matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
        return matrix

    def multiplesteps_in_actiondir(self,action, num_actionsteps=5):
        '''
        this should perform 5 times the action that was choosen
        :param action:action that we want to perform multiple times
        :param num_actionsteps: number of steps with this action we take
        :return:
        '''
        for i in range(num_actionsteps):
            self.env.state, reward, done, info= self.env.step(action)
        return self.env.state, reward, done

    def tuple_to_scalar(self, num_buckets= NUM_BUCKETS):
        '''
        method that transforms a 4tuple input into a scalar
        :param state: 4tuple state as input, gets transformed to a scalar
        :return: scalar state
        '''
        potence = np.math.log(num_buckets, 10)  # log with basis 10
        scalar_state =  (100 ** potence) * self.env.bucketindex[0] + (10 ** potence) * self.env.bucketindex[1] + (
                    1 ** potence) * self.env.bucketindex[2]
        #scalar_state = 1000 * self.env.bucketindex[0] + 100 * self.env.bucketindex[1] + 10 * self.env.bucketindex[2] + 1 * self.env.bucketindex[3]
        '''
        tuple = [scalar_state / 1000, scalar_state % 1000, scalar_state % 100,
                 scalar_state % 10]  # this maps back scalars to tuplestate
        '''
        return scalar_state

    def own_policy_method(self, maxit= NUM_STATES):
        '''
        this policy just simply moves left neg angle and right for a pos angle
        neg angle meaning pole to the left
        :return:
        '''
        policy= np.zeros(NUM_STATES)
        for i in maxit:
            if self.env.state[3]<0:
                action=0
            else:
                action=1
        self.env.step(action)
        policy[i]= action
        self.write_csv(policy, file_name='ownpolicy_greedy_30buckets.csv')
        return policy

        #self.write_csv(policy, file_name='policy_greedy.csv')




    def playgame(self, maxiteration=1000):
        '''
        in here play the game
        call readuserinput and play that multiple times
        maxiteration: maximum number of actions we can choose
        :return:rendered image
        '''
        done=False
        while not done:
            #read in the policy (as the actions) from the document
            #actions= self.read_csv(inpath='/home/anjak/Dokumente/valueiteration_cartpole_policy_30buckets.csv') #TODO qlearning
            #actions= self.read_csv(inpath='/home/anjak/Dokumente/policyiterationpolicy_30b_boundaryresetfunc.csv')
            #actions = self.read_csv(inpath='/home/anjak/Dokumente/valueiteration_cartpole_policy_30buckets.csv')  # TODO valueit
            #actions = self.read_csv(inpath='/home/anjak/Dokumente/policyiterationpolicy_10b.csv')  # TODO policyit
            actions = self.read_csv(inpath='/home/anjak/Dokumente/ql_cartpole_policy_50b_1e7ep.csv')  # TODO qlearning
            #actions= self.read_csv(inpath='/home/anjak/Dokumente/policyiterationpolicy_50b_boundaryreset_gamma099.csv')


            #TODO comment this out when we want a greedy policy moving left when angle neg, moving right otw.
            #if self.env.state[2]<0:
             #   action=0
            #elif self.env.state[2]>=0:
             #   action=1


            #action= self.read_user_input()
            #if done:
             #   self.env.reset() #this that we do not call step when done
            #print('before discretizing: pos after action {} of state is {}'.format(action, self.env.new_state))
            self.env.new_state, self.env.bucketindex= self.env.discretize(self.env.state)
            bucketindexscalar= int(self.tuple_to_scalar(NUM_BUCKETS))
           # print('newstate is {}'.format(self.env.new_state))
            print(' bucketindex is {} and bucketindexscalar {}'.format(self.env.bucketindex, bucketindexscalar))
            action = int(actions[bucketindexscalar]) #TODO changed
            self.env.state, reward, done = self.multiplesteps_in_actiondir(action, num_actionsteps=1)
            #TODO: seems not to change because the environment always sets the state to be the discretized one and this
            #makes it not change
            print('reward: {}, state is {} done var is {}'.format(reward,self.env.state, done))
            print('action is {}'.format(action))
            print('state: {}'.format(self.env.state))
            #TODO print out policy, valuefunction, action, current reward, done variable etc. 
            self.env.leftright=action #TODO added for leftrightrendering

            self.env.render()
            time.sleep(.5)
            #this if and elif is just for debugging
            if done:
                print('done=True and state is {}'.format(self.env.state))
            elif self.env.state[0]<-2.4 or self.env.state[0]> 2.4:
                print('x position out of valid range')
                print('pos of state is {}'.format(self.env.state))

        #return


if __name__ == '__main__':
    game= GameCartPole()
    game.playgame()






