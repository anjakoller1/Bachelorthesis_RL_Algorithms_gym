import numpy as np
import gym
from gym import wrappers
import time
from pprint import pprint

THETA = 1e-10  # convergence_boundary
MAX_ITERATIONS = 1000


def run_episode(env, policy, gamma, render=False):
    '''
    runs an episode and returns the total reward
    :param env: environment we consider
    :param policy: policy we take
    :param gamma: discountfactor
    :param render: boolean it is true we render every episode
    :return: total_reward achieved over all the episodes
    '''

    obs = env.reset()
    total_reward = 0
    done = False
    steps=0
    stepsarray= []
    while not done:
        if render:
            env.render()
        obs, reward, done , info = env.step(int(policy[obs]))
        #total_reward += (gamma * reward)  #discounted reward
        total_reward += reward

        steps +=1
        stepsarray.append(steps)
        steps_mean= np.mean(stepsarray)
        print('mean umber of steps per episode is {}'.format(steps_mean))
    return total_reward


def score_eval(env, policy, gamma, n_episodes = 100):
    scores = [run_episode(env, policy, gamma, render= False) for episode in range(n_episodes)]
    return np.mean(scores)
# high score is good means total reward is high


def extract_policy(env, V, gamma):
    '''

    :param env: environment we act in
    :param V: Value function
    :param gamma: discountfactor
    :return: policy which gives the best reward
    '''
   # extracts the policy given a value function
    policy = np.zeros(env.nS)
    for s in range(env.nS):
        q_sa = np.zeros(env.nA)
        for a in range(env.nA):
            q_sa[a] = sum([p * (r + gamma * V[s_new]) for p, s_new, r, _ in  env.P[s][a]])
        policy[s] = np.argmax(q_sa)
    return policy

def policy_eval(env, V, policy, gamma, theta = THETA):
    '''

    :param env: environment name [gym environment]
    :param policy: policy [array]
    :param gamma: discount factor [float]
    :return: computes the value function under the policy iteratively, returns value function V
    '''

    while True:

        delta=0
        for state in range(env.nS):
            v_old = np.copy(V[state])  # store old value function
            policy_val = policy[state] # to compute v value for the policy
            V[state] = sum([p * (r + gamma * V[s_new]) for p, s_new, r, _ in env.P[state][policy_val]])
            delta = max(delta,(np.abs(v_old-V[state])))
        if (delta <= theta):
            # value converged
            break
    return V


def policy_improvement(env, gamma, use_zeros=True, theta=THETA, max_iterations=MAX_ITERATIONS):
    '''

    :param env:environment we act in
    :param gamma: discountfactor
    :param use_zeros:
    :param theta:
    :param max_iterations:
    :return:policy and number of rounds until convergence (convergence_rounds)
    '''
    #policy improvement step
    #policy = np.random.choice(env.nA, size=(env.nS))  # initialize a random policy
    policy = np.zeros(env.nS) if use_zeros else np.ones(env.nS)
    #policy = np.ones(env.nS) --> with ones initialized more likely to end up in hole
    # tried out different initializations (random or zero or ones --> no difference)
    V = np.zeros(env.nS)
    for i in range(max_iterations):
        V = policy_eval(env, V, policy, gamma, theta=theta)
        new_policy = extract_policy(env, V, gamma)
        if np.all(policy == new_policy):
            convergence_rounds = i+1
            print('Policy-Iteration converges after {} rounds'.format(convergence_rounds))
            break
        policy = new_policy
    return policy, convergence_rounds


def env_test(env_name, gamma, theta=THETA, max_iterations=MAX_ITERATIONS):
    '''

    :param env_name: environment name, e.g. 'Taxi-v2'
    :param gamma: discountfactor
    :param theta: convergence_boundary parameter
    :param max_iterations:maximal iterations of policy improvement
    :return: convergence_rounds (number of rounds it needs until convergence), mean_score (mean of total reward) and total time
    '''
    print('Running %s with gamma=%s' % (env_name, gamma))
    print('Initial state:')
    env = gym.make(env_name).env
    env.reset()
    env.render()
    start_time = time.time()
    optimal_policy, convergence_rounds = policy_improvement(env, gamma=gamma)
   # print('optimal policy is {}'.format(optimal_policy))
    scores = score_eval(env, optimal_policy, gamma=gamma)
    mean_score = np.mean(scores)
    total_time = time.time() - start_time
    print('Average scores = {}'.format(mean_score))
    print('Runtime of the algorithm {}'.format(total_time))
    print('Final state:')
    env.render()
    return convergence_rounds, mean_score, total_time, optimal_policy


if __name__ == '__main__':

    env_name = 'FrozenLake-v0'
    #envs= ['Taxi-v2', 'FrozenLake-v0', 'FrozenLake8x8-v0']
    gammas = [0.2, 0.4, 0.6, 0.8, 0.9, 0.99]
    statistics_dict = {}
    for gamma in gammas:
        convergence_rounds, mean_score, total_time, optimal_policy = env_test(env_name=env_name, gamma=gamma)
        statistics_dict[gamma] = ['Convergence rounds: {}'.format(convergence_rounds),
                                  'Mean_score: {}'.format(mean_score),
                                'Total time: {}'.format(total_time),
                                  'Policy: {}'.format(optimal_policy)]

    print('Gamma results:')
    pprint(statistics_dict)
