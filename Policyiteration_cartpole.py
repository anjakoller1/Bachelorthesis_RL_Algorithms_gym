import numpy as np
import gym
from gym import wrappers
import time
from pprint import pprint
import os
from cartpole_new import CartPoleNewEnv

THETA = 0.0001  # convergence_boundary
MAX_ITERATIONS = 100000
SEP = ';'
PATH = '/home/anjak/Dokumente'


class CartPolepolicyit(object):
    def __init__(self):
        self.env= CartPoleNewEnv()
        self.state= self.env.reset()
        self.new_state, self.new_state_index= self.env.discretize(self.env.state)
        self.obs_dim= np.size(self.env.state)
        self.num_buckets= self.env.num_buckets
        self.num_actions= self.env.action_space.n
        self.num_states= pow(self.num_buckets, self.obs_dim-1)
        self.upper_bounds= self.env.upper_bounds
        self.lower_bounds= self.env.lower_bounds


    def write_csv(self,matrix, file_name):

        outpath = os.path.join(PATH,file_name)
        np.savetxt(outpath, matrix,delimiter=SEP)


    def read_csv(self,inpath):
        matrix = np.loadtxt(open(inpath, "rb"), delimiter=SEP, skiprows=0)
        return matrix

    def multiplesteps_in_actiondir(self,action, num_actionsteps):
        '''
        this should perform 5 times the action that was choosen
        :param action:action that we want to perform multiple times
        :param num_actionsteps: number of steps with this action we take
        :return:
        '''
        for i in range(num_actionsteps):
            self.env.state, reward, done, info= self.env.step(action)
        return self.env.state, reward, done, info



    def run_episode(self,policy,V,Q,T, render=False):
        '''
        runs an episode and returns the total reward
        :param env: environment we consider
        :param policy: policy we take
        :param gamma: discountfactor
        :param render: boolean it is true we render every episode
        :return: total_reward achieved over all the episodes
        '''
        total_reward=0
        self.new_state= self.env.reset()
        self.env.state_discretized, self.env.state_discretized_index= self.env.discretize(self.new_state)
        self.env.state_discretized_index= int(np.floor(self.env.tuple_to_scalar()))
        self.env.set_state(self.env.state_discretized_index)
        obsindex= self.env.state_discretized_index
        done= self.env.is_done()
        steps=0

        while not done:
            if render:
                self.env.render()

            action_to_take= int(round(policy[obsindex]))
            print('S:{}, V:{}, Q: {}, P:{}'.format(obsindex, V[obsindex], Q[obsindex], policy[obsindex]))
            self.new_state, reward, done, info= self.env.step(action_to_take)
            self.env.state_discretized, self.env.state_discretized_index= self.env.discretize(self.new_state)
            self.env.state_discretized_index= int(np.floor(self.env.tuple_to_scalar()))

            newindex= self.env.state_discretized_index

            if T[obsindex][action_to_take]>-1:
                assert(T[obsindex][action_to_take]==newindex)
            else:
                T[obsindex][action_to_take]= newindex

            obsindex= newindex

            if done:
                assert(self.env.is_done())
                break
            self.env.set_state(obsindex)
            total_reward += reward
            steps +=1

            if (total_reward >=195):   #TODO: stop after 195 rewards in 100 consecutive trials
                print('you won the game')
                return
        return total_reward


    def score_eval(self,policy, V,Q,T, n_episodes = 100):
        #TODO put render=True if you want to see the rendering

        scores = [self.run_episode(policy,V,Q,T,render=False) for episode in range(n_episodes)]
        return scores



    def extract_policy(self,env, V, gamma):
        '''

        :param env: environment we act in
        :param V: Value function
        :param gamma: discountfactor
        :return: policy which gives the best reward
        '''
       # extracts the policy given a value function
        policy = np.zeros(self.num_states)
        for s in range(self.num_states):
            q_sa = np.zeros(self.num_actions)
            for a in range(self.num_actions):
                p=1
                self.multiplesteps_in_actiondir(a, num_actionsteps=1) #TODO change here number of actions in actiondir
                new_state, reward, done, info= env.step(a)
                if done:
                    env.reset()
                    break

                new_state, newstateindex = self.discretize(new_state)
                newstateindex = int(self.tuple_to_scalar(newstateindex))
                temp = V[newstateindex]  # to debug
                q_sa[a]= p * (reward + gamma * temp)
            policy[s] = np.argmax(q_sa)
          #  print('policy[s]: {}'.format(policy[s]))
        return policy

    def policy_eval(self,V,policy, gamma, maxiteration=MAX_ITERATIONS, theta=THETA):
        '''

        :param env:environment
        :param gamma: discount factor
        :param maxiteration: maximum number of iterations
        :param theta:convergence_boundary parameter set to THETA
        :return: Value function V
        '''
        convergence_rounds = 0
        policy = np.zeros(self.num_states)
        V = np.zeros(self.num_states, dtype=float)
        Q = np.zeros((self.num_states, self.num_actions), dtype=float)
        T = np.negative(np.ones((self.num_states, self.num_actions), dtype=int))

        for i in range(maxiteration):
            delta = 0
            temp_update = np.zeros(self.num_actions)

            for s in range(
                    self.num_states):  # TODO: for every state in STATES (redefine env.nS) and reset state in the env impl
                v = np.copy(V[s])  # store previous value function
                # self.new_state_index= s
                old_state = s
                # TODO check if done= True --> if yes skip action loop and leave valuefunction going to 0
                # TODO: modify the check for being done so that does not take into account the xvalue
                self.env.set_state(old_state)
                if self.env.is_done():
                    continue  # this skips the iteration if done= true but continues with the next iteratoin

                for action in range(self.num_actions):
                    # s= self.env.set_state(old_state)
                    self.env.set_state(old_state)
                    # self.state = s
                    probability = 1.0
                    # self.env.state_discretized, reward, done, info= cartpoleobj.multiplesteps_in_actiondir(action,num_actionsteps=1)
                    self.new_state, reward, done, info = self.env.step(action)

                    self.env.state_discretized, self.env.state_discretized_index = self.env.discretize(self.new_state)
                    self.env.state_discretized_index = int(np.floor(self.env.tuple_to_scalar()))
                    temp = V[self.env.state_discretized_index]
                    temp_update[action] = probability * (reward + (gamma * temp))
                    # temp_update[action] = probability*(reward +(gamma * V[new_state_index])) #sum up for all newstates and rewards
                    Q[s][action] = temp_update[action]

                    if T[old_state][action] > -1:
                        assert (T[old_state][action] == self.env.state_discretized_index)
                    else:
                        T[old_state][action] = self.env.state_discretized_index

                    if self.env.is_done():  # TODO changed here
                        assert (done)
                        self.env.reset()
                        break

                policy[s] = np.argmax(temp_update)
                V[s] = max(temp_update)  # max(q_sa)
                delta = max(delta, np.abs(v - V[s]))
                i += 1

            if (delta <= theta):  # when V converges
                temp1 = V[s]  # to debug only
                print('V[state] is {}'.format(temp1))
                print('v is {}'.format(v))
                print('valueiteration converged')
                convergence_rounds = i + 1
                # print('Value iteration converged at iteration number {}'.format(i + 1))
                break
            convergence_rounds = i + 1
        # print('policy[{}]: {}'.format(s,policy[s]))
        # print('Value function is {}'.format(V))

        return V,  policy, Q, T

    def policy_improvement(self, gamma, use_zeros=True, theta=THETA, max_iterations=MAX_ITERATIONS):
        '''

        :param env:environment we act in
        :param gamma: discountfactor
        :param use_zeros:
        :param theta:
        :param max_iterations:
        :return:policy and number of rounds until convergence (convergence_rounds)
        '''

        policy = np.zeros(self.num_states) if use_zeros else np.ones(self.num_states)
        V = np.zeros(self.num_states)
        for i in range(max_iterations):
            V, new_policy, Q, T = self.policy_eval(V, policy, gamma, theta=theta)
            #V, new_policy, Q, T = self.policy_eval(gamma, theta=theta)
            #new_policy = self.extract_policy #not needed as we already get policy from policyeval
            if np.all(policy == new_policy):
                convergence_rounds = i+1
                print('Policy-Iteration converges after {} rounds'.format(convergence_rounds))
                break
            #only for testing
            #convergence_rounds = i+1
            policy = new_policy
            i += 1 #TODO remove
        return policy, convergence_rounds,V, Q, T


    def env_test(self, gamma):
        '''

        :param env_name: environment name, e.g. 'Taxi-v2'
        :param gamma: discountfactor
        :param theta: convergence_boundary parameter
        :param max_iterations:maximal iterations of policy improvement
        :return: convergence_rounds (number of rounds it needs until convergence), mean_score (mean of total reward) and total time
        '''
        print('Running CartPole with gamma=%s' % (gamma))
        start_time = time.time()
        optimal_policy, convergence_rounds,V, Q, T = self.policy_improvement(gamma=gamma)
        self.write_csv(optimal_policy, file_name= 'policyiterationpolicy_50b_lowtheta.csv') #this writes us the policy in a csv file
        self.write_csv(V, file_name= 'Valuefunction_policyiterationcartpole_50b_lowtheta.csv')
       #print('optimal policy is {}'.format(optimal_policy))
        scores= self.score_eval(optimal_policy, V, Q, T)
        #mean_score = np.mean(scores) #TODO problem with mean on scores --> change return to mean_score instead of scores
        total_time = time.time() - start_time
        return convergence_rounds, scores, total_time, optimal_policy


if __name__ == '__main__':

    cartpoleobj= CartPolepolicyit()
    #gammas = [0.2, 0.4, 0.6, 0.8, 0.9]
    gammas= [0.99] #TODO: comment above out, with one gamma we are faster in testing
    statistics_dict = {}
    for gamma in gammas:
        convergence_rounds, scores, total_time, optimal_policy = cartpoleobj.env_test(gamma=gamma)
        statistics_dict[gamma] = ['Convergence rounds: {}'.format(convergence_rounds),
                                    'Mean_score: {}'.format(scores),
                                'Total time: {}'.format(total_time),
                                    'Policy: {}'.format(optimal_policy)]

    print('Gamma results:')
    pprint(statistics_dict)
